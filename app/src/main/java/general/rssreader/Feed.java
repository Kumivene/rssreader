package general.rssreader;

import android.os.Parcelable;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

/**
 * Created by Daniel on 24.9.2015.
 */
public class Feed implements Runnable,Serializable{
    private ArrayList<FeedItem> feedItemArrayList = new ArrayList<>();
    private String feedName = null;
    private URL feedURL;


    public Feed(String feedurlitext){
        try {
            this.feedURL = new URL(feedurlitext);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }



    }
    public void run(){
        this.feedName = generateTitle(this.feedURL);
        populateFeedItems(feedURL);

    }

    public void addFeedItem(FeedItem nfi){

        this.feedItemArrayList.add(nfi);

    }

    public void addFeedTitle(String fn){
        this.feedName = fn;
    }

    public int printSize(){
        return feedItemArrayList.size();
    }
    @Override
    public String toString(){
        return this.feedName;
    }

    public ArrayList<FeedItem> returnFIArrayList(){
        return feedItemArrayList;
    }

    public void clearArrayList(){
        feedItemArrayList.clear();
    }


    public String getFeedURL(){
        return this.feedURL.toString();
    }

    private String generateTitle(URL u){


        try {

            XmlPullParserFactory factory = null;
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            InputStream rssInput = u.openStream();
            xpp.setInput(rssInput, null);

            int eventType = xpp.getEventType();
            eventType = xpp.next();

            while (feedName == null) {
                if(eventType == XmlPullParser.START_TAG){
                    if(xpp.getName().equals("title")){
                        feedName = xpp.nextText();
                    }
                    else {
                        Log.e("jee","jee");
                    }
                }
                eventType = xpp.next();
            }
        }
            catch (XmlPullParserException e) {
                e.printStackTrace();
                Log.e("kaatuu",e.toString());
            } catch (IOException e) {
            e.printStackTrace();
        }
        return feedName;
    }

    public void removeSingleFeedItem(FeedItem f){
        this.feedItemArrayList.remove(f);
    }
    public void addSingleFeedItem(FeedItem f){
        this.feedItemArrayList.add(f);
    }


    private void populateFeedItems(URL u){


        XmlPullParserFactory factory = null;
        try {
            InputStream rssInput = feedURL.openStream();
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            FeedItem nfi = null;

            xpp.setInput(rssInput, null);

            int eventType = xpp.getEventType();
            eventType = xpp.next();

            while (eventType != XmlPullParser.END_DOCUMENT) {

                if (eventType == XmlPullParser.START_TAG){

                    if(xpp.getName().equals("item")){

                        nfi = new FeedItem();


                    }
                    if (nfi != null) {

                        if (xpp.getName().equals("title")) {
                            nfi.addFeedItemTitle(xpp.nextText());

                        } else if (xpp.getName().equals("link")) {
                            nfi.addFeedItemLink(xpp.nextText());

                        }else if (xpp.getName().equals("pubDate")){
                            nfi.addFeedItemDate(xpp.nextText().substring(0,25));

                        }
                        else {

                        }
                    }


                }
                else if (eventType == XmlPullParser.END_TAG){

                    if(xpp.getName().equals("item")){
                        feedItemArrayList.add(nfi);
                        Log.e("perseri", " " + Feed.this.printSize());
                        Log.e("perseri",nfi.toString());
                        nfi = null;

                    }
                }
                eventType = xpp.next();

            }

            Log.e("parseri valmis", "" + this.printSize());
            Log.e("perseri valmis", "" + this.toString());
            Log.e("parser", "End document");



        }
        catch (MalformedURLException e){
            Log.e("error ",e.toString());
        }
        catch (XmlPullParserException e) {
            e.printStackTrace();
            Log.e("kaatuu",e.toString());
        }


        catch (IOException e) {
            Log.e("eror ", e.toString());
        }

        Log.e("loppu","help");


    }

    public void refreshFeedItems(){
        this.feedItemArrayList.clear();
        //populateFeedItems(this.feedURL);
    }

    @Override
    public boolean equals(Object f){
    Log.e("pim","pom");
        if(f != null && f instanceof Feed){
            return this.feedURL.equals(((Feed) f).getFeedURL());

        }
        else {
            return false;
        }
    }
    @Override
    public int hashCode() {
        Log.e("pom","pim");
        return this.feedURL.hashCode();
    }

}


