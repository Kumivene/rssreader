package general.rssreader;

import android.util.Log;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by Daniel on 1.10.2015.
 */
public class User{

    private transient static User instance = new User();
    private ArrayList<Feed> userFeedList = new ArrayList<>();
    private ArrayList<Observer> observerList = new ArrayList<>();
    private ArrayList<FeedItem> favoriteFeeds = new ArrayList<>();
    private ArrayList<FeedItem> readItems = new ArrayList<>();
    private ArrayList<FeedItem> allFeedItemsEver = new ArrayList<>();


    private User(){}

    public static User getInstance(){
        return instance;
    }
    public ArrayList<Feed> getUserFeedList(){
        return this.userFeedList;

    }
    public void addFeedToUser(Feed f){
        this.userFeedList.add(f);
        updateAllObservers();
    }

    public void replaceFeeds(ArrayList<Feed> rfl/*,ArrayList<FeedItem> replacedFav,ArrayList<FeedItem> replacedRead*/){
        this.userFeedList = rfl;
        /*this.readItems = replacedRead;
        this.favoriteFeeds = replacedFav;*/
        for(int i = 0; i < rfl.size();i++){
            try {
                Feed replaceFeed = new Feed(rfl.get(i).getFeedURL());
                Thread t = new Thread(replaceFeed);
                t.start();
                Log.e("user thread", "go");
                t.join();
                Log.e("user thread", "end");
                if(i<this.userFeedList.size()){
                    this.userFeedList.set(i,replaceFeed);
                }
                else{
                    this.userFeedList.add(replaceFeed);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        updateAllObservers();
    }
    public void replaceFavs(ArrayList<FeedItem> f){
        this.favoriteFeeds = f;
        updateAllObservers();
    }

    public void removeFeedFromUser(int i){
        Log.e("uuseri", "removed");
        userFeedList.remove(i);
        updateAllObservers();
        Log.e("uuseri", "removed2");
    }

    public boolean containsDupeFavorite(FeedItem fi){
        boolean returnV = false;
        if(favoriteFeeds.size() != 0){
            for(int i = 0; i<favoriteFeeds.size();i++){
                if (favoriteFeeds.get(i).getFeedItemLink().equals(fi.getFeedItemLink())){
                    Log.e("favorites","tried adding dupe");
                    returnV = false;
                    break;
                }
                else{
                   returnV = true;
                    Log.e("favorites","no dupe detected");
                }
            }
        }else{
            returnV = true;
        }return  returnV;
    }

    public void addFavoriteFeedToUser(FeedItem f){
        if(containsDupeFavorite(f)){
            this.favoriteFeeds.add(f);
            updateAllObservers();
            Log.e("favotrites","added" + f.toString());
        }
    }

    public void removeFavoriteFeedFromUser(int i){
        this.favoriteFeeds.remove(i);
        updateAllObservers();
    }

    public void markAsRead(FeedItem f,int i){
        this.readItems.add(f);
        this.userFeedList.get(i).removeSingleFeedItem(f);
        updateAllObservers();
    }

    public void unmarkAsRead(FeedItem f,int i){
        this.readItems.remove(f);
        this.userFeedList.get(i).addSingleFeedItem(f);
        updateAllObservers();
    }

    public ArrayList<FeedItem> getReadItems(){
        return this.readItems;
    }

    public ArrayList<FeedItem> getFavoritesFromUser(){
        return this.favoriteFeeds;
    }

    public void registerObserver(Observer o){
        observerList.add(o);
    }

    public void updateAllObservers(){
        for(int i = 0; i<observerList.size();i++){
            observerList.get(i).updateObserver();
        }
    }

    public boolean containsDupe(Feed ff){
        boolean returnV = true;
        for(int i = 0; i<this.userFeedList.size();i++){
            if(this.userFeedList.get(i).getFeedURL().equals(ff.getFeedURL())){
                returnV = false;
                break;
            }
            else{
                returnV = true;

            }
        }return returnV;
    }

    public ArrayList<FeedItem> combineAllFeedItems() {

        TreeSet<FeedItem> tempallfeeds = new TreeSet<>();
        for (int i = 0; i < this.userFeedList.size(); i++) {

            tempallfeeds.addAll(userFeedList.get(i).returnFIArrayList());
        }
        this.allFeedItemsEver.clear();
        this.allFeedItemsEver.addAll(tempallfeeds);
        return this.allFeedItemsEver;
    }
}
