package general.rssreader;

import android.util.Log;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.Integer.parseInt;

/**
 * Created by Daniel on 24.9.2015.
 */
public class FeedItem implements Serializable,Comparable{

    private String feedItemTitle = "default";
    private String feedItemLink = "default";
    private String feedItemDate = "default";

    public FeedItem(){
        this.feedItemTitle = "default";
    }

    public void addFeedItemTitle(String nfit){
        this.feedItemTitle = nfit;
    }
    public void addFeedItemLink(String nfil){
        this.feedItemLink = nfil;
    }
    public void addFeedItemDate(String nfid){
        this.feedItemDate = nfid;
    }

    public String getFeedItemTitle(){ return this.feedItemTitle; }
    public String getFeedItemLink(){ return this.feedItemLink; }
    public String getFeedItemDate(){return this.feedItemDate;}

    @Override
    public String toString(){
        return (this.feedItemDate +"  -  " +this.feedItemLink + "  -  " + this.feedItemTitle);
    }

    @Override
    public int compareTo(Object another) {

        FeedItem comparedFeed = (FeedItem) another;
        int returnV;

            String time2S = comparedFeed.getFeedItemDate().substring(17, 19) + comparedFeed.getFeedItemDate().substring(20, 22);
            String time1S = this.feedItemDate.substring(17, 19) + this.feedItemDate.substring(20, 22);
            int time1 = parseInt(time1S);
            int time2 = parseInt(time2S);
            String date1S = this.feedItemDate.substring(5,7);
            String date2S = comparedFeed.getFeedItemDate().substring(5,7);
            int date1 = parseInt(date1S);
            int date2 = parseInt(date2S);

            if(date1 < date2){
                time1 =time1- 3000;
            }
            else if(date1 > date2){
                time2 =time2- 3000;
            }
            else {

            }

            Log.e("sorting","dates: " + date1 + " " + date2);
            Log.e("sorting for:"," " + time1 + "against " + time2);

            if (time1 == time2){
                returnV = 0;
                Log.e("sorting","equal");
            }
            else if (time1 < time2){
                returnV = -1;
                Log.e("sorting", "smaller");
            }
            else {
                returnV = 1;
                Log.e("sorting","bigger");
            }


        return returnV;
    }
}
