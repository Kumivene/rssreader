package general.rssreader;

/**
 * Created by Daniel on 1.10.2015.
 */
public interface Observer {
    public void updateObserver();
}
