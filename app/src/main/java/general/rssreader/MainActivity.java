
package general.rssreader;

        import android.app.AlertDialog;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.content.SharedPreferences;
        import android.graphics.Color;
        import android.graphics.drawable.ColorDrawable;
        import android.support.v4.widget.DrawerLayout;
        import android.support.v7.app.ActionBarDrawerToggle;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.ContextMenu;
        import android.view.Gravity;
        import android.view.Menu;
        import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ListView;
        import android.widget.RelativeLayout;

        import java.io.FileInputStream;
        import java.io.FileNotFoundException;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.ObjectInputStream;
        import java.io.ObjectOutputStream;
        import java.io.StreamCorruptedException;
        import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements Observer {


    private FeedListAdapter feedListAdapter1;
    private ListView listi;
    private User uuseri = User.getInstance();

    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    ArrayList<String> drawerItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        uuseri.registerObserver(MainActivity.this);


        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#009688")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // DrawerLayout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);

        mDrawerList = (ListView) findViewById(R.id.navList);
        drawerItems.add("Browse Feeds");
        drawerItems.add("Add Feed");
        drawerItems.add("Favorites");
        drawerItems.add("Read Items");
        drawerItems.add("All Feeds");
        DrawerListAdapter adapter = new DrawerListAdapter(this, drawerItems);
        mDrawerList.setAdapter(adapter);

        //drawerToggle for fancy burger menu
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.hello_world, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d("drawerCloed", "onDrawerClosed: " + getTitle());

                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener()

           {
               @Override
               public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                   selectItemFromDrawer(position);
               }
           }

        );


        //UI element initialization

        this.listi = (ListView)findViewById(R.id.list);
        this.feedListAdapter1 = new FeedListAdapter(this,R.layout.activity_main,uuseri.getUserFeedList());
        this.listi.setAdapter(feedListAdapter1);
        registerForContextMenu(this.listi);


        listi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), SingleFeedView.class);
                intent.putExtra("passedFeed", position);
                startActivity(intent);

            }
        });



    }

    @Override //title bar mikä sisältää refreshit sun muut
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_single_feed_view, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //mitä tapahtuu kun title barin nappeja painetaan
    //se mitä painetaan määrittää if lause
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.refresh) {
            uuseri.updateAllObservers();
            Log.e("refresh,","painettu");
        }
        else if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }
    //Fance Burger menu animation
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    //Popup menu
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.list) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            //menu.setHeaderTitle(Countries[info.position]);
            uuseri.removeFeedFromUser(info.position);
        }
    }
    /*@Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();

        uuseri.removeFeedFromUser(info.position);




        return true;
    }*/



    public void updateWindow(){
        this.runOnUiThread(new Runnable() {
            public void run(){
                feedListAdapter1.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void updateObserver() {
        updateWindow();
    }

    //drawer buttons
    private void selectItemFromDrawer(int position) {
        if(position == 1){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setTitle("Add Feed");
            //alert.setMessage("Message");

            final EditText input = new EditText(this);
            alert.setView(input);

            alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    Log.e("add popup",input.getText().toString());
                    if (!input.getText().toString().equals("")) {
                        Feed uusifeed = new Feed(input.getText().toString());
                        if(uuseri.getUserFeedList().size() != 0) {
                            if (uuseri.containsDupe(uusifeed)) {

                                uuseri.addFeedToUser(uusifeed);
                                Thread t2 = new Thread(uusifeed);
                                t2.start();
                            }
                        }
                        else{
                            uuseri.addFeedToUser(uusifeed);
                            Thread t2 = new Thread(uusifeed);
                            t2.start();
                        }

                    }

                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });

            alert.show();
        }

        else if (position == 0){
            /*Intent intentHome = new Intent(getApplicationContext(), MainActivity.class);
            intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intentHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intentHome);
*/
        }
        else if (position == 2) {
            Intent intent = new Intent(getApplicationContext(), FavoriteFeedItemsView.class);

            startActivity(intent);
        }
        else if (position == 3){
            Intent intent = new Intent(getApplicationContext(), ReadItemsView.class);
            startActivity(intent);
        }
        else if(position == 4){
            Intent intent = new Intent(getApplicationContext(),AllFeedItemsView.class);
            startActivity(intent);
        }
        // Close the drawer
        mDrawerLayout.closeDrawer(mDrawerPane);
    }
    @Override
    protected void onPause(){
        super.onPause();

        FileOutputStream outputStream;

        FileOutputStream outputStream2;

        FileOutputStream outputStream3;
        try{
            outputStream = openFileOutput("RssSavedInstance", Context.MODE_PRIVATE);
            ObjectOutputStream ous = new ObjectOutputStream(outputStream);
            ous.writeObject(User.getInstance().getUserFeedList());

            outputStream2 = openFileOutput("RssSavedFavs", Context.MODE_PRIVATE);
            ObjectOutputStream ous2 = new ObjectOutputStream(outputStream2);
            ous.writeObject(User.getInstance().getFavoritesFromUser());

            outputStream3 = openFileOutput("RssSavedRead", Context.MODE_PRIVATE);
            ObjectOutputStream ous3 = new ObjectOutputStream(outputStream3);
            ous.writeObject(User.getInstance().getReadItems());

            outputStream.close();
            ous.close();

            outputStream2.close();
            ous2.close();

            outputStream3.close();
            ous3.close();

            Log.e("file","write");
        } catch (FileNotFoundException e) {
            Log.e("erorr", e.toString());
        } catch (IOException e) {
            Log.e("erorr", e.toString());
        }


    }
    @Override
    protected void onResume(){
        super.onResume();
        FileInputStream in = null;
        FileInputStream in2 = null;
        FileInputStream in3 = null;
        try {
            in = openFileInput("RssSavedInstance");
            ObjectInputStream obin = new ObjectInputStream(in);
            ArrayList<Feed> temp1 = ((ArrayList<Feed>)obin.readObject());
            in.close();
            obin.close();

/*
            in2 = openFileInput("RssSavedFavs");
            ObjectInputStream obin2 = new ObjectInputStream(in2);
            ArrayList<FeedItem> temp2 = (ArrayList<FeedItem>)obin2.readObject();
            in2.close();
            obin2.close();

            in3 = openFileInput("RssSavedRead");
            ObjectInputStream obin3 = new ObjectInputStream(in3);
            ArrayList<FeedItem> temp3 = (ArrayList<FeedItem>)obin3.readObject();
            in3.close();
            obin3.close();

            uuseri.replaceFeeds(temp1,temp2,temp3);
            in.close();
            obin.close();
            in2.close();
            obin2.close();
            in3.close();
            obin3.close();*/
            uuseri.replaceFeeds(temp1);
            this.feedListAdapter1 = new FeedListAdapter(this,R.layout.activity_main,uuseri.getUserFeedList());
            listi.setAdapter(this.feedListAdapter1);
            uuseri.updateAllObservers();
            //uuseri.replaceFavs(temp2);

            Log.e("file","read on resume");
        } catch (FileNotFoundException e) {
            Log.e("erorr", e.toString());
        } catch (StreamCorruptedException e) {
            Log.e("erorr", e.toString());
        } catch (IOException e) {
            Log.e("erorr", e.toString());
        }catch (ClassNotFoundException e) {
            Log.e("erorr", e.toString());
        }
    }
}
