package general.rssreader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Daniel on 28.9.2015.
 */
public class FeedListAdapter extends ArrayAdapter<Feed>{
    private ArrayList<Feed> feedArrayList;
    private Context context;

    public FeedListAdapter(Context context, int resource, ArrayList<Feed> feedArrayLists){
        super(context, resource,feedArrayLists);
        this.feedArrayList = feedArrayLists;
        this.context = context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = li.inflate(R.layout.cell_layout_feed, null);

        TextView cellTextTitle = (TextView)convertView.findViewById(R.id.cellTextViewTitle);

        cellTextTitle.setText(this.feedArrayList.get(position).toString());

        return convertView;
    }



}
