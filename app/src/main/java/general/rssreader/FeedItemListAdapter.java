package general.rssreader;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Daniel on 28.9.2015.
 */
public class FeedItemListAdapter extends ArrayAdapter<FeedItem>{
    private ArrayList<FeedItem> feedArrayList;
    private Context context;
    private User uuseri = User.getInstance();

    public FeedItemListAdapter(Context context, int resource, ArrayList<FeedItem> feedArrayLists){
        super(context, resource, feedArrayLists);
        this.feedArrayList = feedArrayLists;
        this.context = context;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = li.inflate(R.layout.cell_layout, null);

        TextView cellTextTitle = (TextView)convertView.findViewById(R.id.cellTextViewTitle);
        TextView cellTextPub = (TextView)convertView.findViewById(R.id.cellTextViewDate);
        //final ImageButton favButton = (ImageButton)convertView.findViewById(R.id.favoriteButton);

        cellTextTitle.setText(this.feedArrayList.get(position).getFeedItemTitle());
        cellTextPub.setText(this.feedArrayList.get(position).getFeedItemDate());

        return convertView;
    }






}
