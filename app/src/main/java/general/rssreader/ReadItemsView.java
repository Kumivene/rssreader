package general.rssreader;

import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

/**
 * Created by Daniel on 5.10.2015.
 */
public class ReadItemsView extends AppCompatActivity implements Observer{




    private FeedItemListAdapter feedListAdapter1;
    private ListView listi;
    private User uuseri = User.getInstance();
    private int passedExtra;
    ArrayList<String> drawerItems = new ArrayList<>();

    ListView mDrawerList;
    RelativeLayout mDrawerPane;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_items_view);


        getSupportActionBar().setTitle("Read Items");
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#009688")));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        uuseri.registerObserver(ReadItemsView.this);
        Bundle extras = getIntent().getExtras();

        this.listi = (ListView)findViewById(R.id.list);

        this.feedListAdapter1 = new FeedItemListAdapter(this,R.layout.activity_single_feed_view,(((uuseri.getReadItems()))));
        listi.setAdapter(feedListAdapter1);
        //getSupportActionBar().setTitle("");
        registerForContextMenu(this.listi);


        // DrawerLayout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);

        mDrawerList = (ListView) findViewById(R.id.navList);
        drawerItems.add("Browse Feeds");
        drawerItems.add("Add Feed");
        drawerItems.add("Favorites");
        drawerItems.add("Read Items");
        drawerItems.add("All Feeds");
        DrawerListAdapter adapter = new DrawerListAdapter(this, drawerItems);
        mDrawerList.setAdapter(adapter);

        //drawerToggle for fancy burger menu
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.hello_world, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Log.d("drawerCloed", "onDrawerClosed: " + getTitle());

                invalidateOptionsMenu();
            }
        };

        // Drawer Item click listeners
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener()

                                           {
                                               @Override
                                               public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                   selectItemFromDrawer(position);
                                               }
                                           }

        );





        listi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FeedItem tempfeed = (FeedItem)(listi.getItemAtPosition(position));
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tempfeed.getFeedItemLink()));
                startActivity(browserIntent);
                //uuseri.markAsRead(tempfeed,passedExtra);
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_single_feed_view, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.refresh) {
            uuseri.updateAllObservers();
            Log.e("update nappi","painettu");
        }
        else if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public void updateWindow(){

        this.runOnUiThread(new Runnable() {
            public void run() {
                feedListAdapter1.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void updateObserver() {
        updateWindow();
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId()==R.id.list) {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            //menu.setHeaderTitle(Countries[info.position]);
            String[] menuItems = new String[]{"Favourite","Copy Link","Share","Mark Unread"};

            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();


        if(menuItemIndex == 0){
            uuseri.addFavoriteFeedToUser(uuseri.getReadItems().get(info.position));

        }
        else if (menuItemIndex == 1){
            ClipboardManager clippy = (ClipboardManager)this.getSystemService(Context.CLIPBOARD_SERVICE);
            clippy.setText(uuseri.getUserFeedList().get(passedExtra).returnFIArrayList().get(info.position).getFeedItemLink());
        }
        else if (menuItemIndex == 2){
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Check out this link!");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, uuseri.getUserFeedList().get(passedExtra).returnFIArrayList().get(info.position).getFeedItemLink());
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }
        else if(menuItemIndex == 3){
            uuseri.unmarkAsRead(uuseri.getReadItems().get(info.position),passedExtra);
            Log.e("unmar read",uuseri.getUserFeedList().get(passedExtra).returnFIArrayList().get(info.position).toString());
        }



        return true;
    }


    //what happens when side menu item is clicked
    private void selectItemFromDrawer(int position) {
        if(position == 1){
            AlertDialog.Builder alert = new AlertDialog.Builder(this);

            alert.setTitle("Add Feed");
            //alert.setMessage("Message");

            final EditText input = new EditText(this);
            alert.setView(input);

            alert.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    Log.e("add popup",input.getText().toString());
                    if (!input.getText().toString().equals("")) {
                        Feed uusifeed = new Feed(input.getText().toString());

                        if (!uuseri.getUserFeedList().contains(uusifeed)) {

                            uuseri.addFeedToUser(uusifeed);
                            Thread t2 = new Thread(uusifeed);
                            t2.start();
                        }


                    }

                }
            });

            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });

            alert.show();
        }

        else if (position == 0){
            Intent intentHome = new Intent(getApplicationContext(), MainActivity.class);
            intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intentHome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(intentHome);
        }
        else if (position == 2){
            Intent intent = new Intent(getApplicationContext(), FavoriteFeedItemsView.class);
            startActivity(intent);
        }
        else if (position == 3){
            Intent intent = new Intent(getApplicationContext(), ReadItemsView.class);
            startActivity(intent);
        }

        else if(position == 4){
            Intent intent = new Intent(getApplicationContext(),AllFeedItemsView.class);
            startActivity(intent);
        }
        // Close the drawer
        mDrawerLayout.closeDrawer(mDrawerPane);
    }
}
